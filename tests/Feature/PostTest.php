<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Traits\UserHelper;
use Illuminate\Support\Facades\Schema;
use Laravel\Sanctum\PersonalAccessToken;
use Tests\TestCase;

class PostTest extends TestCase
{
    use UserHelper;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_posts()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $post = Post::factory()->create();
        $user->createToken('login');
        $response = $this->actingAs($user)->get('api/post');
        $response->assertStatus(200);
        $response->assertJson(['data' => [
            [
                'title' => $post->title,
                'body' => $post->body,
                'created_by' => $post->user->name,
                'created_at' => $post->created_at->format('j F Y')
            ]
        ]]);
    }

    public function test_get_own_posts()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Admin');
        $post = Post::factory()->create([
            'user_id' => $user->id
        ]);
        $user->createToken('login');
        $response = $this->actingAs($user)->get('api/post/own');
        $response->assertStatus(200);
        $response->assertJson(['data' => [
            [
                'title' => $post->title,
                'body' => $post->body,
                'created_by' => $post->user->name,
                'created_at' => $post->created_at->format('j F Y')
            ]
        ]]);
    }

    public function test_get_detail_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $post = Post::factory()->create();
        $user->createToken('login');
        $response = $this->actingAs($user)->get('api/post/detail/' . $post->id);
        $response->assertStatus(200);
        $response->assertJson([
            'title' => $post->title,
            'body' => $post->body,
            'created_by' => $post->user->name,
            'created_at' => $post->created_at->format('j F Y')
        ]);
    }

    public function test_admin_success_create_a_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Admin');
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/post/create', [
            'title' => 'Fungsi Smartphone di Era Global',
            'body' => 'Adanya smartphone si era globalisasi ini menciptakan berbagai manfaat yang dirasakan oleh banyak orang. Dilihat dari segi sosial, manusia kini mudah berinteraksi tanpa terhalang jarak, bahkan bisa bertatap muka secara langsung melalui fitur video call.'
        ]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success create a post']);
    }

    public function test_contributor_success_create_a_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Contributor');
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/post/create', [
            'title' => 'Fungsi Smartphone di Era Global',
            'body' => 'Adanya smartphone si era globalisasi ini menciptakan berbagai manfaat yang dirasakan oleh banyak orang. Dilihat dari segi sosial, manusia kini mudah berinteraksi tanpa terhalang jarak, bahkan bisa bertatap muka secara langsung melalui fitur video call.'
        ]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success create a post']);
    }

    public function test_default_user_cannot_create_a_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/post/create', [
            'title' => 'Fungsi Smartphone di Era Global',
            'body' => 'Adanya smartphone si era globalisasi ini menciptakan berbagai manfaat yang dirasakan oleh banyak orang. Dilihat dari segi sosial, manusia kini mudah berinteraksi tanpa terhalang jarak, bahkan bisa bertatap muka secara langsung melalui fitur video call.'
        ]);
        $response->assertStatus(403);
        $response->assertJson(['message' => 'Unauthorized']);
    }

    public function test_admin_can_update_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Admin');
        $post = Post::factory()->create();
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/post/update/' . $post->id, [
            'title' => 'Smartphone Jual Cepat'
        ]);
        $response->assertStatus(200);

        $post->refresh();
        $this->assertTrue($post->title == 'Smartphone Jual Cepat');
    }

    public function test_contributor_can_update_own_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Contributor');
        $post = Post::factory()->create([
            'user_id' => $user->id
        ]);
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/post/update/' . $post->id, [
            'title' => 'Smartphone Jual Cepat'
        ]);
        $response->assertStatus(200);

        $post->refresh();
        $this->assertTrue($post->title == 'Smartphone Jual Cepat');
    }

    public function test_contributor_cannot_update_another_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Contributor');
        $post = Post::factory()->create();
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/post/update/' . $post->id, [
            'title' => 'Smartphone Jual Cepat'
        ]);
        $response->assertStatus(400);
        $response->assertJson(['message' => 'You are not own the post']);
    }

    public function test_admin_can_delete_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Admin');
        $post = Post::factory()->create();
        $user->createToken('login');
        $response = $this->actingAs($user)->delete('api/post/delete/' . $post->id);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success delete post']);
        $this->assertDatabaseMissing('posts', $post->toArray());
    }

    public function test_contributor_can_delete_own_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Contributor');
        $post = Post::factory()->create([
            'user_id' => $user->id
        ]);
        $user->createToken('login');
        $response = $this->actingAs($user)->delete('api/post/delete/' . $post->id);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success delete post']);
        $this->assertDatabaseMissing('posts', $post->toArray());
    }
}
