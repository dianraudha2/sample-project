<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Traits\UserHelper;
use Illuminate\Support\Facades\Schema;
use Laravel\Sanctum\PersonalAccessToken;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use UserHelper;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_add_comment()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $post = Post::factory()->create();
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/comment/add', [
            'post_id' => $post->id,
            'body' => 'Hello World!'
        ]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success add comment']);
        $this->assertDatabaseHas('comments', [
            'post_id' => $post->id,
            'body' => 'Hello World!'
        ]);
        $this->assertDatabaseCount('comments', 1);
    }

    public function test_update_comment()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $user->createToken('login');
        $comment = Comment::factory()->create(['user_id' => $user->id]);
        $response = $this->actingAs($user)->post('api/comment/update/' . $comment->id, [
            'body' => 'Hi World!'
        ]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success update comment']);
        $this->assertDatabaseHas('comments', [
            'post_id' => $comment->post->id,
            'user_id' => $user->id,
            'body' => 'Hi World!'
        ]);
    }

    public function test_cannot_update_comment()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $user->createToken('login');
        $comment = Comment::factory()->create();
        $response = $this->actingAs($user)->post('api/comment/update/' . $comment->id, [
            'body' => 'Hi World!'
        ]);
        $response->assertStatus(400);
        $response->assertJson(['message' => 'You are not own the comment']);
    }

    public function test_user_delete_comment()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $user->createToken('login');
        $comment = Comment::factory()->create(['user_id' => $user->id]);
        $response = $this->actingAs($user)->delete('api/comment/delete/' . $comment->id);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success delete comment']);
        $this->assertDatabaseMissing('comments', [
            'post_id' => $comment->post->id,
            'user_id' => $user->id,
            'body' => 'Hello World!'
        ]);
    }

    public function test_contributor_delete_comment_on_his_own_post()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();

        $contributor = $this->createUser('Contributor');
        $contributor->createToken('login');
        $post = Post::create([
            'user_id' => $contributor->id,
            'title' => 'Random title',
            'body' => 'This is the body of post'
        ]);

        $user = $this->createUser('Default');
        $comment = Comment::create([
            'user_id' => $user->id,
            'post_id' => $post->id,
            'body' => 'This is the comment'
        ]);

        $response = $this->actingAs($contributor)->delete('api/comment/delete/' . $comment->id);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success delete comment']);
        $this->assertDatabaseMissing('comments', [
            'post_id' => $comment->post->id,
            'user_id' => $user->id,
            'body' => 'Hello World!'
        ]);
    }

    public function test_contributor_delete_comment_on_another_post_that_his_own_comment()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();

        $contributor = $this->createUser('Contributor');
        $contributor->createToken('login');
        $anotherPost = Post::factory()->create();

        $user = $this->createUser('Default');
        $comment = Comment::create([
            'user_id' => $contributor->id,
            'post_id' => $anotherPost->id,
            'body' => 'This is the comment'
        ]);

        $response = $this->actingAs($contributor)->delete('api/comment/delete/' . $comment->id);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success delete comment']);
        $this->assertDatabaseMissing('comments', [
            'post_id' => $comment->post->id,
            'user_id' => $user->id,
            'body' => 'Hello World!'
        ]);
    }

    public function test_contributor_cannot_delete_comment_on_another_post_that_not_his_own_comment()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Post::truncate();
        Comment::truncate();
        PersonalAccessToken::truncate();

        $contributor = $this->createUser('Contributor');
        $contributor->createToken('login');
        $anotherPost = Post::factory()->create();

        $user = $this->createUser('Default');
        $comment = Comment::create([
            'user_id' => $user->id,
            'post_id' => $anotherPost->id,
            'body' => 'This is the comment'
        ]);

        $response = $this->actingAs($contributor)->delete('api/comment/delete/' . $comment->id);
        $response->assertStatus(400);
        $response->assertJson(['message' => 'You are not own the comment and the post']);
    }
}
