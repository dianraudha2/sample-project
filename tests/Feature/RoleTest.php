<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_roles()
    {
        $response = $this->get('api/roles');
        $response->assertStatus(200);
        $response->assertJson([
            'roles' => [
                ['id' => 1, 'name' => 'Default'],
                ['id' => 2, 'name' => 'Contributor'],
                ['id' => 3, 'name' => 'Admin'],
            ]
        ]);
    }
}
