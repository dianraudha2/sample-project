<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use App\Traits\UserHelper;
use Illuminate\Support\Facades\Schema;
use Laravel\Sanctum\PersonalAccessToken;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use UserHelper;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_success_register()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        $response = $this->post('api/register', [
            'name' => 'Dian Raudha Allisya',
            'email' => 'dian@mail.com',
            'password' => 'asdfasdf',
            'password_confirmation' => 'asdfasdf',
            'role_id' => Role::where('name', 'Default')->first()->id
        ]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success register']);
    }

    public function test_register_email_is_exists()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        $user = $this->createUser('Default');
        $response = $this->post('api/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'asdfasdf',
            'password_confirmation' => 'asdfasdf',
            'role_id' => Role::where('name', 'Default')->first()->id
        ]);
        $response->assertStatus(400);
        $response->assertJson(['message' => 'Email is already exists']);
    }

    public function test_success_login()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $response = $this->post('api/login', [
            'email' => $user->email,
            'password' => 'asdfasdf'
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['token']);
    }

    public function test_login_email_not_exist()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        $response = $this->post('api/login', [
            'email' => 'dian@mail.com',
            'password' => 'asdfasdf'
        ]);
        $response->assertStatus(400);
        $response->assertJson(['message' => 'Email not found']);
    }

    public function test_login_wrong_password()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        $user = $this->createUser('Default');
        $response = $this->post('api/login', [
            'email' => $user->email,
            'password' => 'qwertyuiop'
        ]);
        $response->assertStatus(400);
        $response->assertJson(['message' => 'Wrong password']);
    }

    public function test_change_password_success()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/change-password', [
            'old_password' => 'asdfasdf',
            'new_password' => 'qwertyuiop',
            'new_password_confirmation' => 'qwertyuiop',
        ]);
        $response->assertStatus(200);

        $user->refresh();
        $this->assertTrue(password_verify('qwertyuiop', $user->password));
    }

    public function test_change_password_wrong_old_password()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/change-password', [
            'old_password' => 'qwertyuiop',
            'new_password' => 'asdfasdf',
            'new_password_confirmation' => 'asdfasdf',
        ]);
        $response->assertStatus(400);
        $response->assertJson(['message' => 'Wrong old password']);
    }

    public function test_update_bio()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $user->createToken('login');
        $response = $this->actingAs($user)->post('api/update-bio', [
            'bio' => 'Life is never flat.'
        ]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success update bio']);
    }

    public function test_get_user_profile()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $user = $this->createUser('Default');
        $user->createToken('login');
        $response = $this->actingAs($user)->get('api/profile');
        $response->assertStatus(200);
        $response->assertJson([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'bio' => $user->bio,
            'joined_at' => $user->created_at->format('j F Y')
        ]);
    }
}
