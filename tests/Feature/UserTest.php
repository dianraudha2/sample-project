<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use App\Traits\UserHelper;
use Illuminate\Support\Facades\Schema;
use Laravel\Sanctum\PersonalAccessToken;
use Tests\TestCase;

class UserTest extends TestCase
{
    use UserHelper;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_admin_get_users()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $admin = $this->createUser('Admin');
        $admin->createToken('login');
        $user = $this->createUser('Default');
        $response = $this->actingAs($admin)->get('api/user');
        $response->assertStatus(200);
        $response->assertJson(['data' => [
            [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'bio' => $user->bio,
                'joined_at' => $user->created_at->format('j F Y')
            ]
        ]]);
    }

    public function test_admin_add_new_user()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $admin = $this->createUser('Admin');
        $admin->createToken('login');
        $response = $this->actingAs($admin)->post('api/user/add', [
            'name' => 'Syaiful',
            'email' => 'syaif@mail.com'
        ]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Success create user, please check email for login.']);
        $this->assertDatabaseHas('users', [
            'name' => 'Syaiful',
            'email' => 'syaif@mail.com'
        ]);
    }

    public function test_admin_add_new_user_email_is_already_exist()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $admin = $this->createUser('Admin');
        $admin->createToken('login');
        $user = $this->createUser('Default');
        $response = $this->actingAs($admin)->post('api/user/add', [
            'name' => $user->name,
            'email' => $user->email
        ]);
        $response->assertStatus(400);
        $response->assertJson(['message' => 'Email is already exists']);
    }

    public function test_admin_can_update_user_profile()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        PersonalAccessToken::truncate();
        $admin = $this->createUser('Admin');
        $admin->createToken('login');
        $user = $this->createUser('Default');
        $response = $this->actingAs($admin)->post('api/user/update/' . $user->id, [
            'name' => 'Allisya',
            'role_id' => Role::where('name', 'Contributor')->first()->id,
            'bio' => 'Always be happy :)'
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'name' => 'Allisya',
            'role_id' => Role::where('name', 'Contributor')->first()->id,
            'bio' => 'Always be happy :)'
        ]);
    }

    public function test_admin_can_delete_user()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate(); 
        PersonalAccessToken::truncate();
        $admin = $this->createUser('Admin');
        $admin->createToken('login');
        $user = $this->createUser('Default');
        $response = $this->actingAs($admin)->delete('api/user/delete/' . $user->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('users', [
            'name' => $user->name,
            'role_id' => Role::where('name', 'Default')->first()->id
        ]);
    }
}
