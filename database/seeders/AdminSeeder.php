<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin Post',
            'email' => 'admin@mail.com',
            'password' => Hash::make('adminadmin'),
            'role_id' => Role::where('name', 'Admin')->first()->id
        ]);
    }
}
