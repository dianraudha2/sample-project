<?php

namespace App\Transformers;

use App\Models\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Comment $comment)
    {
        return [
            'id' => $comment->id,
            'body' => $comment->body,
            'created_by' => $comment->user->name,
            'created_at' => $comment->created_at->format('j F Y'),
            'edited' => $comment->updated_at > $comment->created_at ? true : false
        ];
    }
}
