<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Role;

/**
 * @group Roles
 *
 * Api's for get roles
 */
class RoleController extends Controller
{
    /**
     * Get Roles
     *
     * This endpoint allow you to get list roles
     *
     * @response {
     *  "roles": [
     *        {
     *            "id": 1,
     *            "name": "Default"
     *        },
     *        {
     *            "id": 2,
     *            "name": "Contributor"
     *        },
     *        {
     *            "id": 3,
     *            "name": "Admin"
     *        }
     *    ]
     * }
     */
    public function getRoles()
    {
        $roles = Role::select('id', 'name')->get();
        return response()->json(['roles' => $roles]);
    }
}
