<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePostRequest;
use App\Models\Post;
use App\Transformers\PostTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

/**
 * @group Post
 *
 * Api's for manage post
 */
class PostController extends Controller
{
    /**
     * Get All Posts
     *
     * This endpoint allow you to get all posts
     *
     * @response {
     *    "data": [
     *        {
     *            "id": 2,
     *            "title": "Bahaya Jamur Hutan",
     *            "body": "Berikut bahaya dari jamur hutan adalah sebagai berikut",
     *            "created_by": "Admin Post",
     *            "created_at": "3 January 2023",
     *            "comments": [
     *                {
     *                    "id": 1,
     *                    "body": "Ini komentar pertama saya",
     *                    "created_by": "Ida Conroy",
     *                    "created_at": "4 January 2023"
     *                }
     *            ]
     *        }
     *    ]
     * }
     */
    public function getAllPosts()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        $fractal = new Manager();
        $resource = new Collection($posts, new PostTransformer());
        return response()->json($fractal->createData($resource)->toArray());
    }

    /**
     * Get Own Posts
     *
     * This endpoint allow you to get all your own posts
     *
     * @response {
     *    "data": [
     *        {
     *            "id": 2,
     *            "title": "Bahaya Jamur Hutan",
     *            "body": "Berikut bahaya dari jamur hutan adalah sebagai berikut",
     *            "created_by": "Admin Post",
     *            "created_at": "3 January 2023",
     *            "comments": [
     *                {
     *                    "id": 1,
     *                    "body": "Ini komentar pertama saya",
     *                    "created_by": "Ida Conroy",
     *                    "created_at": "4 January 2023"
     *                }
     *            ]
     *        }
     *    ]
     * }
     */
    public function getOwnPosts()
    {
        $user = auth()->user();
        $posts = Post::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        $fractal = new Manager();
        $resource = new Collection($posts, new PostTransformer());
        return response()->json($fractal->createData($resource)->toArray());
    }

    /**
     * Get Detail Post
     *
     * This endpoint allow you to get detail of the post
     *
     * @response {
     *    "id": 2,
     *    "title": "Bahaya Jamur Hutan",
     *    "body": "Berikut bahaya dari jamur hutan adalah sebagai berikut",
     *    "created_by": "Admin Post",
     *    "created_at": "3 January 2023",
     *    "comments": [
     *        {
     *            "id": 1,
     *            "body": "Ini komentar pertama saya",
     *            "created_by": "Ida Conroy",
     *            "created_at": "4 January 2023"
     *        }
     *    ]
     * }
     */
    public function getDetailPost($id)
    {
        $post = Post::find($id);
        return response()->json((new PostTransformer())->transform($post));
    }

    /**
     * Create Post
     *
     * This endpoint allow you to create new post
     *
     * @bodyParam title string required The title of the post. Example: Here Is The Title
     * @bodyParam body text required The body of the post. Example: This is the body of the post.
     *
     * @response {
     *      "message": "Success create a post"
     * }
     *
     * @response 403 {
     *      "message": "Unauthorized"
     * }
     */
    public function create(CreatePostRequest $request)
    {
        $user = auth()->user();
        Post::create([
            'user_id' => $user->id,
            'title' => $request->title,
            'body' => $request->body
        ]);
        return response()->json(['message' => 'Success create a post']);
    }

    /**
     * Update Post
     *
     * This endpoint allow you to update your post
     *
     * @urlParam id string required The id of the post. Example: 1
     * @bodyParam title string required The title of the post. Example: Here Is The Title
     * @bodyParam body text required The body of the post. Example: This is the body of the post.
     *
     * @response {
     *      "message": "Success update a post"
     * }
     *
     * @response 400 {
     *      "message": "Post not exist"
     * }
     *
     * @response 400 {
     *      "message": "You are not own the post"
     * }
     *
     * @response 403 {
     *      "message": "Unauthorized"
     * }
     */
    public function update($id)
    {
        $user = auth()->user();
        $post = Post::find($id);
        $checkPost = $this->checkPost($user, $post);
        if ($checkPost !== true) {
            return $checkPost;
        }

        $post->update(request()->except('user_id'));
        return response()->json(['message' => 'Success update post']);
    }

    /**
     * Delete Post
     *
     * This endpoint allow you to delete your post
     *
     * @urlParam id string required The id of the post. Example: 1
     *
     * @response {
     *      "message": "Success delete a post"
     * }
     *
     * @response 400 {
     *      "message": "Post not exist"
     * }
     *
     * @response 400 {
     *      "message": "You are not own the post"
     * }
     *
     * @response 403 {
     *      "message": "Unauthorized"
     * }
     */
    public function delete($id)
    {
        $user = auth()->user();
        $post = Post::find($id);
        $checkPost = $this->checkPost($user, $post);
        if ($checkPost !== true) {
            return $checkPost;
        }

        $post->delete();
        return response()->json(['message' => 'Success delete post']);
    }

    function checkPost($user, $post)
    {
        if (is_null($post)) {
            return response()->json(['message' => 'Post not exist'], 400);
        }

        if ($user->role->name == 'Contributor') {
            if ($post->user_id !== $user->id) {
                return response()->json(['message' => 'You are not own the post'], 400);
            }
        }
        return true;
    }
}
