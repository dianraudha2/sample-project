<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Mail\RegisterMail;
use App\Models\Role;
use App\Models\User;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use League\Fractal\Manager;
use Illuminate\Support\Str;
use League\Fractal\Resource\Collection;

/**
 * @group User Management
 *
 * Api's for Admin manage user
 */
class UserController extends Controller
{
    /**
     * Get All Users
     *
     * This endpoint allow admin to get all users
     *
     * @response {
     *      "data": [
     *        {
     *            "id": 2,
     *            "name": "Allisya",
     *            "email": "sya@mail.com",
     *            "role": "Contributor",
     *            "bio": "Always be happy :)",
     *            "joined_at": "3 January 2023"
     *        },
     *        {
     *            "id": 3,
     *            "name": "Bobby Wisoky",
     *            "email": "Henriette_Crist15@hotmail.com",
     *            "role": "Default",
     *            "bio": null,
     *            "joined_at": "3 January 2023"
     *        }
     *    ]
     * }
     */
    public function getUsers()
    {
        $users = User::orderBy('created_at')->get()->except(auth()->user()->id);
        $fractal = new Manager();
        $resource = new Collection($users, new UserTransformer());
        return response()->json($fractal->createData($resource)->toArray());
    }

    /**
     * Get Detail User
     *
     * This endpoint allow admin to get detail user
     *
     * @urlParam id string required The id of user. Example: 1
     *
     * @response {
     *    "id": 2,
     *    "name": "Allisya",
     *    "email": "sya@mail.com",
     *    "role": "Contributor",
     *    "bio": "Always be happy :)",
     *    "joined_at": "3 January 2023"
     * }
     */
    public function getDetailUser($id)
    {
        $user = User::find($id);
        return response()->json((new UserTransformer())->transform($user));
    }

    /**
     * Register New User
     *
     * This endpoint allow admin to register new user
     *
     * @bodyParam name string required The name of the user. Example: John Doe
     * @bodyParam email email required The email of the user. Example: asdfasdf@gmail.com
     *
     * @response {
     *      "message": "Success create user, please check email for login."
     * }
     *
     * @response 400 {
     *      "message": "Email is already exists"
     * }
     */
    public function registerUser(RegisterUserRequest $request)
    {
        $emailIsExists = User::where('email', $request->email)->first();
        if (!is_null($emailIsExists)) {
            return response()->json(['message' => 'Email is already exists'], 400);
        }

        $password = Str::random(16);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($password),
            'role_id' => Role::where('name', 'Default')->first()->id
        ]);
        Mail::to($request->user())->send(new RegisterMail($user, $password));
        return response()->json(['message' => 'Success create user, please check email for login.']);
    }

    /**
     * Update User
     *
     * This endpoint allow admin to update user
     *
     * @urlParam id string required The id of user. Example: 1
     * @bodyParam name string nullable The name of the user. Example: John Doe
     * @bodyParam bio text nullable The bio of the user. Example: This is John Doe bio
     * @bodyParam role_id integer nullable The role_id of the user. Example: 1
     *
     * @response {
     *      "message": "Success update user"
     * }
     *
     * @response 400 {
     *      "message": "User not found"
     * }
     */
    public function updateUser($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json(['message' => 'User not found'], 400);
        }
        $user->update(request()->except('email'));
        return response()->json(['message' => 'Success update user']);
    }

    /**
     * Delete User
     *
     * This endpoint allow admin to delete user
     *
     * @urlParam id string required The id of user. Example: 1
     *
     * @response {
     *      "message": "Success delete user"
     * }
     *
     * @response 400 {
     *      "message": "User not found"
     * }
     */
    public function deleteUser($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json(['message' => 'User not found'], 400);
        }
        $user->delete();
        return response()->json(['message' => 'Success delete user']);
    }
}
