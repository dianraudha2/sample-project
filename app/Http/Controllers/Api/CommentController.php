<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddCommentRequest;
use App\Models\Comment;
use App\Models\Post;

/**
 * @group Comments
 *
 * Api's for get roles
 */
class CommentController extends Controller
{
    /**
     * Add Comment
     *
     * This endpoint allow you to add new comment
     *
     * @bodyParam post_id integer required The post id want to comment. Example: 1
     * @bodyParam body text required The body of comment. Example: This is my first comment
     *
     * @response {
     *    "message": "Success add comment"
     * }
     *
     * @response 400 {
     *    "message": "Post not exist"
     * }
     */
    public function add(AddCommentRequest $request)
    {
        $user = auth()->user();
        $post = Post::find($request->post_id);
        $checkPost = $this->checkPost($post);
        if ($checkPost !== true) {
            return $checkPost;
        }

        Comment::create([
            'user_id' => $user->id,
            'post_id' => $post->id,
            'body' => $request->body
        ]);
        return response()->json(['message' => 'Success add comment']);
    }

    /**
     * Update Comment
     *
     * This endpoint allow you to update your comment
     *
     * @urlParam id integer required The comment id want to update. Example: 1
     * @bodyParam body text required The body of comment. Example: This is my first comment
     *
     * @response {
     *    "message": "Success update comment"
     * }
     *
     * @response 400 {
     *    "message": "Comment not exist"
     * }
     *
     * @response 400 {
     *    "message": "You are not own the comment"
     * }
     */
    public function update($id)
    {
        $user = auth()->user();
        $comment = Comment::find($id);
        $checkComment = $this->checkComment($user, $comment);
        if ($checkComment !== true) {
            return $checkComment;
        }
        if ($comment->user_id !== $user->id) {
            return response()->json(['message' => 'You are not own the comment'], 400);
        }
        $comment->update(request()->except('post_id, user_id'));
        return response()->json(['message' => 'Success update comment']);
    }

    /**
     * Delete Comment
     *
     * This endpoint allow you to delete your comment
     *
     * @urlParam id integer required The comment id want to delete. Example: 1
     *
     * @response {
     *    "message": "Success delete comment"
     * }
     *
     * @response 400 {
     *    "message": "Comment not exist"
     * }
     *
     * @response 400 {
     *    "message": "You are not own the comment"
     * }
     *
     *  @response 400 {
     *    "message": "You are not own the comment and the post"
     * }
     */
    public function delete($id)
    {
        $user = auth()->user();
        $comment = Comment::find($id);
        $checkComment = $this->checkComment($user, $comment);
        if ($checkComment !== true) {
            return $checkComment;
        }

        $checkPost = $this->checkPost($comment->post);
        if ($checkPost !== true) {
            return $checkPost;
        }

        $comment->delete();
        return response()->json(['message' => 'Success delete comment']);
    }

    function checkPost($post)
    {
        if (is_null($post)) {
            return response()->json(['message' => 'Post not exist'], 400);
        }
        return true;
    }

    function checkComment($user, $comment)
    {
        if (is_null($comment)) {
            return response()->json(['message' => 'Comment not exist'], 400);
        }

        switch ($user->role->name) {
            case 'Default':
                if ($comment->user_id !== $user->id) {
                    return response()->json(['message' => 'You are not own the comment'], 400);
                }
                break;

            default:
                if ($comment->user_id !== $user->id) {
                    if ($comment->post->user_id !== $user->id) {
                        return response()->json(['message' => 'You are not own the comment and the post'], 400);
                    }
                } elseif ($comment->user_id == $user->id) {
                    return true;
                } else {
                    return response()->json(['message' => 'You are not own the comment'], 400);
                }
                break;
        }
        return true;
    }
}
