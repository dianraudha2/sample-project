<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UpdateBioRequest;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Transformers\UserTransformer;

/**
 * @group Authentication
 *
 * Api's for authenticate user
 */
class AuthController extends Controller
{
    /**
     * Register
     *
     * This endpoint allow you to register
     *
     * @bodyParam name string required The name of the user. Example: John Doe
     * @bodyParam email email required The email of the user. Example: asdfasdf@gmail.com
     * @bodyParam password string required The password of the user. Example: asdfasdf
     * @bodyParam password_confirmation string required The password_confirmation of the user. Example: asdfasdf
     *
     * @response {
     *      "message": "Success register"
     * }
     *
     * @response 400 {
     *      "message": "Email is already exists"
     * }
     */
    public function register(RegisterRequest $request)
    {
        $emailIsExists = User::where('email', $request->email)->first();
        if (!is_null($emailIsExists)) {
            return response()->json(['message' => 'Email is already exists'], 400);
        }

        $defaultRole = Role::where('name', 'Default')->first();
        $password = Hash::make($request->password);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'role_id' => $defaultRole->id
        ]);
        return response()->json(['message' => 'Success register']);
    }

    /**
     * Login
     *
     * This endpoint allow you to login
     *
     * @bodyParam email email required The email of the user. Example: asdfasdf@gmail.com
     * @bodyParam password string required The password of the user. Example: asdfasdf
     *
     * @response {
     *      "token": "1|KFvpiTXKeuPfZWTEg7eiWjmKsnkt48LAhLatgNvy"
     * }
     *
     * @response 400 {
     *      "message": "Email not found"
     * }
     *
     * @response 400 {
     *      "message": "Wrong password"
     * }
     */
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if (is_null($user)) {
            return response()->json(['message' => 'Email not found'], 400);
        }
        $passwordCheck = password_verify($request->password, $user->password);
        if ($passwordCheck) {
            $token = $user->createToken('login');
            return response()->json(['token' => $token->plainTextToken]);
        }
        return response()->json(['message' => 'Wrong password'], 400);
    }

    /**
     * Change Password
     *
     * This endpoint allow you to change password
     *
     * @bodyParam old_password string required The password of the user. Example: asdfasdf
     * @bodyParam new_password string required The new password of the user. Example: qwertyuiop
     * @bodyParam new_password_confirmation string required The new password of the user. Example: qwertyuiop
     *
     * @response {
     *      "message": "Success change password"
     * }
     *
     * @response 400 {
     *      "message": "Wrong old password"
     * }
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        $passwordCheck = password_verify($request->old_password, $user->password);
        if ($passwordCheck) {
            $user->update([
                'password' => Hash::make($request->new_password)
            ]);
            return response()->json(['message' => 'Success change password']);
        }
        return response()->json(['message' => 'Wrong old password'], 400);
    }

    /**
     * Update Bio
     *
     * This endpoint allow you to update bio
     *
     * @bodyParam bio text required The bio of the user. Example: I am a user
     *
     * @response {
     *      "message" => "Success update bio"
     * }
     */
    public function updateBio(UpdateBioRequest $request)
    {
        $user = auth()->user();
        $user->update(['bio' => $request->bio]);
        return response()->json(['message' => 'Success update bio']);
    }

    /**
     * Profile
     *
     * This endpoint allow you to get your profile
     *
     * @response {
     *    "id": 2,
     *    "name": "Allisya",
     *    "email": "sya@mail.com",
     *    "role": "Contributor",
     *    "bio": "Always be happy :)",
     *    "joined_at": "3 January 2023"
     * }
     */
    public function profile()
    {
        $user = auth()->user();
        return response()->json((new UserTransformer())->transform($user));
    }
}
