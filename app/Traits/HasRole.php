<?php

namespace App\Traits;

Trait HasRole {
    function hasRole($role)
    {
        if ($this->role !== null) {
            if ($this->role->name == $role) {
                return true;
            }
        }
        return false;
    }
}
