<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Step by Step to run the project

- **first, clone the project**
- **open the project terminal**
- **composer update**
- **cp .env.example .env**
- **php artisan key:generate**
- **php artisan migrate**
- **php artisan db:seed**
- **php artisan serve**
- **enjoy**

### Default Account Admin

- Email: admin@mail.com
- Password: adminadmin

**See the documentation at** http://localhost:8000/docs
