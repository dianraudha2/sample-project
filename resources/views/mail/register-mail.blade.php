<x-mail::message>
# Welcome to Sample Project, {{ $user->name }}

Please login with your new account. <br>
Email : {{ $user->email }} <br>
Password : {{ $password }} <br>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
